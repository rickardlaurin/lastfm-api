
/*
 * GET home page.
 */

var movee   = require('./movee-utils')
,   request = require('request')
,   moment  = require('moment');

var page = 867;
var i = 0;

var url = 'http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=hpbeliever&api_key=59a34f30f3c5163f936e755463780ad2&format=json&limit=200&page={page}';


function insertIntoDb (page, index) {
  request(url.replace('{page}', page), function (err, response, body) {
    var body = JSON.parse(body);

    movee.mongoConnect(function (err, collection) {
      body.recenttracks.track.map(function (song) {
        collection.insert(song, function (err, ok) {
        });
        i++;
      });

      console.log('page ' + page + ' complete');
      if (page < 867) {
        setTimeout(function () {
          i = 0;
          page++;
          insertIntoDb(page, i)
        }, 10000);
      } else {
        console.log('ALL FINISHED!');
      }
    });
  });
}

/**
 * Gets all movies from db
 * @param  {obj} req 
 * @param  {obj} res 
 */
exports.index = function (req, res) {
  movee.mongoConnect(function (err, collection) {
    var query = req.query.q;

    var find = {
      "artist.#text": query || 'Vertical Horizon'
    };

    collection.find(find).sort({_id:1}).limit(10000).toArray(function (error, songs) {
      var sortedSongs = {};

      songs.map(function (song) {
        if (!sortedSongs[moment(song.date["#text"]).year()]) {
          sortedSongs[moment(song.date["#text"]).year()] = [];
        }

        if (!sortedSongs[moment(song.date["#text"]).year()][[moment(song.date["#text"]).month()]]) {
          sortedSongs[moment(song.date["#text"]).year()][moment(song.date["#text"]).month()] = [];
        }

        sortedSongs[moment(song.date["#text"]).year()][moment(song.date["#text"]).month()].push(song);
      });

      // res.render('index', { songs:sortedSongs });
      res.send(sortedSongs);
    });
  });
};